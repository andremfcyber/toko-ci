<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $profile->companyName;?>">
    <meta name="author" content="<?php echo $profile->companyName;?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('asset/img/uploads/banner/') ?><?php if(!empty($icontitle->image)){echo $icontitle->image;}?>"/>    
    <title><?php echo $profile->companyName;?></title>
    <link href="<?php echo base_url('asset/') ?>vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/') ?>vendors/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/') ?>dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/') ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style>
        body {
            height: 100vh;
            background-image:url(https://dpkukm.banyuasinkab.go.id/wp-content/uploads/sites/240/2020/12/17973908.jpg);
            background-size:cover;
            /* background: #9053c7;
            background: -webkit-linear-gradient(-135deg,#c850c0,#4158d0);
            background: -o-linear-gradient(-135deg,#c850c0,#4158d0);
            background: -moz-linear-gradient(-135deg,#c850c0,#4158d0);
            background: linear-gradient(-135deg,#c850c0,#4158d0); */
        }
        fieldset {
            min-width: 0;
            padding: 10px;
        }

        .input-group-addon { 
            color: #337ab7!important;
            background-color: #e8f0fe;
            border: 1px solid #e8f0fe; 
        }
        .form-control { 
            height: 38px;
            border: 2px solid #e8f0fe!important;
            box-shadow:none!important;
        }
        .panel-default>.panel-heading {
            border-top: 5px solid #e8f0fe!important;
            color: #333; 
            background-color: #ffffff;
            border-color: #ddd;
            padding-bottom: 32px;
        }

        .login-panel {
            border-radius: 19px;
            margin-top: 25%;
        }

        .alert-smg {
            margin-top: -37px;
            margin-bottom: 1px;
            color: #fff;
            border:none!important;
            background: #f44336;
        }

        /* Ripple effect */
        .ripple {
            border:none!important;
            background-position: center;
            transition: background 0.8s;
            box-shadow:none!important;
        }
        .ripple:hover {
            background: #47a7f5 radial-gradient(circle, transparent 1%, #47a7f5 1%) center/15000%;
        }
        .ripple:active {
            background-color: #6eb9f7;
            background-size: 100%;
            transition: background 0s;
        }

         
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                  
                <div class="login-panel panel panel-default ">
                     <?php if ($this->session->flashdata('MSG')) { ?>
                        <div class="alert alert-danger alert-smg" >
                           <i class="fa fa-info-circle fa-lg"></i> <?= $this->session->flashdata('MSG') ?>
                        </div>
                    <?php } ?>
                    <div class="panel-heading text-center">
                        <h1><b>WELCOME</b></h1>
                        <h3 class="panel-title text-center"><?php echo $profile->companyName;?> Dashboard<br> Please Sign In
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?php echo base_url('d/User/do_login')?>" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" ><i class="fa fa-user" id="user"></i></a> </span>
                                        <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group"> 
                                        <span class="input-group-addon"><a onclick="myFunction()" title="Show Password" style=" cursor: pointer;">
                                        <i class="fa fa-eye" id="eyes"></i></a> 
                                        </span> 
                                        <input class="form-control" placeholder="Password" name="password" type="password" id="password" value="" required="">
                                    </div> 
                                </div>
                                <hr>
                                <button class="btn btn-lg btn-info btn-block ripple" type="submit"><i class="fa fa-sign-in"></i> Login</button>
                                <hr>
                                <div class="text-center s-text8">
                                    Copyright © 2022 All rights reserved <br> Toko Koperasi 
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url('asset/') ?>vendors/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendors/metisMenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>/dist/js/sb-admin-2.js"></script>
    <script>
			function myFunction() {
				var x = document.getElementById("password");
				var gambar = document.getElementById("eyes");
				if (x.type === "password") {
				x.type = "text";
				gambar.className = "fa fa-eye-slash";
				} else {
				x.type = "password";
				gambar.className = "fa fa-eye";
				}
			}
		</script>
</body>
</html>
